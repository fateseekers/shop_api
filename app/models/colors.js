/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('colors', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    code: {
      type: DataTypes.STRING(7),
      allowNull: false
    },
    good_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'colors'
  });
};
