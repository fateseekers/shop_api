/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('categories', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'goods',
        key: 'category_id'
      }
    },
    en: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    ru: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    parent_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'categories'
  });
};
