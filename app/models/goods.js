/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('goods', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'sizes',
        key: 'good_id'
      }
    },
    en: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    ru: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    category_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    specification: {
      type: DataTypes.JSON,
      allowNull: false
    }
  }, {
    tableName: 'goods'
  });
};
